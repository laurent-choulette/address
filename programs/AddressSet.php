<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009-2019 by CANTICO ({@link http://www.cantico.fr})
 * @copyright Copyright (c) 2019 by Cap Welton ({@link https://www.capwelton.com})
 */


namespace Capwelton\App\Address;

/**
 * @property ORM_TextField          $street
 * @property ORM_StringField        $postalCode
 * @property ORM_StringField        $city
 * @property ORM_StringField        $cityComplement
 * @property ORM_StringField        $state
 * @property ORM_StringField        $longitude
 * @property ORM_StringField        $latitude
 *
 * @method Address                  get()
 * @method Address                  request()
 * @method Address[]|\ORM_Iterator  select()
 * @method Address                  newRecord()
 */
abstract class AddressSet extends \app_RecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);

        $App = $this->App();

        $this->setPrimaryKey('id');

        $this->setDescription('Address');

        $this->addFields(
            ORM_TextField('street')
                ->setDescription('Number / pathway'),
            ORM_StringField('postalCode', 10)
                ->setDescription('Zip code'),
            ORM_StringField('city', 60)
                ->setDescription('City'),
            ORM_StringField('cityComplement', 60)
                ->setDescription('City complement (CEDEX...)'),
            ORM_StringField('state', 60)
                ->setDescription('State/Region'),
            ORM_StringField('longitude', 60)
                ->setDescription('Longitude'),
            ORM_StringField('latitude', 60)
                ->setDescription('Latitude')
        );

        $this->hasOne('country', $App->CountrySetClassName());
    }
}
